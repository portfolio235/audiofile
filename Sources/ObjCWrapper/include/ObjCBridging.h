//
//  ObjCBridging.h
//
//
//  Created by Massimiliano Bonafede on 14/05/24.
//

#ifndef Header_h
#define Header_h
#import <Foundation/Foundation.h>

@interface ObjCBridging : NSObject

/// Loads an audio file from a given file path.
/// - Returns: true if the file was successfully loaded
- (BOOL)load:(NSString *) filePath;

/// - Returns: the sample rate
- (int32_t)getSampleRate;

/// - Returns: the number of audio channels in the buffer
- (int)getNumChannels;

/// - Returns: true if the audio file is mono
- (BOOL)isMono;

/// - Returns: true if the audio file is stereo
- (BOOL)isStereo;

/// - Returns: the bit depth of each sample
- (int)getBitDepth;

/// - Returns: the number of samples per channel
- (int)getNumSamplesPerChannel;

/// - Returns: the length in seconds of the audio file based on the number of samples and sample rate
- (double)getLengthInSeconds;

/// Prints a summary of the audio file to the console
- (void)printSummary;

/// Sets the audio buffer to a given number of channels and number of samples per channel. This will try to preserve the existing audio, adding zeros to any new channels or new samples in a given channel.
- (void)setAudioBufferSize:(int)numChannels numSamples:(int)numSamples;

/// Sets the number of samples per channel in the audio buffer. This will try to preserve the existing audio, adding zeros to new samples in a given channel if the number of samples is increased.
- (void)setNumSamplesPerChannel:(int)numSamples;

/// Sets the number of channels. New channels will have the correct number of samples and be initialised to zero
- (void)setNumChannels:(int)numChannels;

/// Sets the bit depth for the audio file. If you use the save() function, this bit depth rate will be used
- (void)setBitDepth:(int)numBitsPerSample;

/// Sets the sample rate for the audio file. If you use the save() function, this sample rate will be used
- (void)setSampleRate:(int32_t)newSampleRate;

/// Sets whether the library should log error messages to the console. By default this is true
- (void)shouldLogErrorsToConsole:(BOOL)logErrors;
@end

#endif /* Header_h */
