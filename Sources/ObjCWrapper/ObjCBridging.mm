//
//  ObjCBridging.m
//  
//
//  Created by Massimiliano Bonafede on 14/05/24.
//

#import <Foundation/Foundation.h>
#import <include/ObjCBridging.h>
#include "AudioFile.h"

typedef std::vector<std::vector<float>> AudioBuffer;

@interface ObjCBridging()
@property AudioFile<float> audio;
@end

@implementation ObjCBridging
@synthesize audio;

// MARK: - Private

/// Saves an audio file to a given file path.
/// - Returns: true if the file was successfully saved
- (BOOL)save:(NSString *)filePath format:(AudioFileFormat)format {
    return audio.save(filePath.UTF8String, format);
}

/// Set the audio buffer for this AudioFile by copying samples from another buffer.
/// - Returns: true if the buffer was copied successfully.
- (BOOL)setAudioBuffer:(AudioBuffer)audioBuffer {
    return audio.setAudioBuffer(audioBuffer);
}

// MARK: - Public

- (BOOL)load:(NSString *) filePath {
    return audio.load(filePath.UTF8String);
}

- (AudioBuffer)samples {
    return audio.samples;
}

- (int32_t)getSampleRate {
    return audio.getSampleRate();
}

- (int)getNumChannels {
    return audio.getNumChannels();
}

- (BOOL)isMono {
    return audio.isMono();
}

- (BOOL)isStereo {
    return audio.isStereo();
}

- (int)getBitDepth {
    return audio.getBitDepth();
}

- (int)getNumSamplesPerChannel {
    return audio.getNumSamplesPerChannel();
}

- (double)getLengthInSeconds {
    return audio.getLengthInSeconds();
}

- (void)printSummary {
    audio.printSummary();
}

- (void)setAudioBufferSize:(int)numChannels numSamples:(int)numSamples {
    audio.setAudioBufferSize(numChannels, numSamples);
}

- (void)setNumSamplesPerChannel:(int)numSamples {
    audio.setNumSamplesPerChannel(numSamples);
}

- (void)setNumChannels:(int)numChannels {
    audio.setNumChannels(numChannels);
}

- (void)setBitDepth:(int)numBitsPerSample {
    audio.setBitDepth(numBitsPerSample);
}

- (void)setSampleRate:(int32_t)newSampleRate {
    audio.setSampleRate(newSampleRate);
}

- (void)shouldLogErrorsToConsole:(BOOL)logErrors {
    audio.shouldLogErrorsToConsole(logErrors);
}

@end
