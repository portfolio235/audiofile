//
//  AudioWrapper.swift
//
//
//  Created by Massimiliano Bonafede on 14/05/24.
//

import Foundation
import ObjCWrapper

public class AudioWrapper {
    
    // MARK: - Properties

    private let bridging: ObjCBridging = ObjCBridging()
    
    // MARK: - Lifecycle

    public init() { }
    
    // MARK: - Methods

    public func load(_ filePath: String) -> Bool {
        return bridging.load(filePath)
    }
    
    /// - Returns: the sample rate
    public func getSampleRate() -> Int32 {
        return bridging.getSampleRate()
    }
    
    /// - Returns: the number of audio channels in the buffer
    public func getNumChannels() -> Int32 {
        return bridging.getNumChannels()
    }
    
    /// - Returns: true if the audio file is mono
    public func isMono() -> Bool {
        return bridging.isMono()
    }
    
    /// - Returns: true if the audio file is stereo
    public func isStereo() -> Bool {
        return bridging.isStereo()
    }
    
    /// - Returns: the bit depth of each sample
    public func getBitDepth() -> Int32 {
        return bridging.getBitDepth()
    }
    
    /// - Returns: the number of samples per channel
    public func getNumSamplesPerChannel() -> Int32 {
        return bridging.getNumSamplesPerChannel()
    }
    
    /// - Returns: the length in seconds of the audio file based on the number of samples and sample rate
    public func getLengthInSeconds() -> Double {
        return bridging.getLengthInSeconds()
    }
    
    /// Prints a summary of the audio file to the console
    public func printSummary() {
        bridging.printSummary()
    }
    
    /// Sets the audio buffer to a given number of channels and number of samples per channel. This will try to preserve the existing audio, adding zeros to any new channels or new samples in a given channel.
    public func setAudioBufferSize(channels: Int32, samples: Int32) {
        bridging.setAudioBufferSize(channels, numSamples: samples)
    }
    
    /// Sets the number of samples per channel in the audio buffer. This will try to preserve the existing audio, adding zeros to new samples in a given channel if the number of samples is increased.
    public func setNumSamplesPerChannel(samples: Int32) {
        bridging.setNumSamplesPerChannel(samples)
    }
    
    /// Sets the number of channels. New channels will have the correct number of samples and be initialised to zero
    public func setNumChannels(channels: Int32) {
        bridging.setNumChannels(channels)
    }
    
    /// Sets the bit depth for the audio file. If you use the save() function, this bit depth rate will be used
    public func setBitDepth(bitsPerSample: Int32) {
        bridging.setBitDepth(bitsPerSample)
    }
    
    /// Sets the sample rate for the audio file. If you use the save() function, this sample rate will be used
    public func setSampleRate(sampleRate: Int32) {
        bridging.setSampleRate(sampleRate)
    }
    
    /// Sets whether the library should log error messages to the console. By default this is true
    public func shouldLogErrors(toConsole logErrors: Bool) {
        bridging.shouldLogErrors(toConsole: logErrors)
    }
}
