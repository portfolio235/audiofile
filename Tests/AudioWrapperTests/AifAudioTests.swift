import XCTest
@testable import SwiftWrapper

final class AifAudioTests: XCTestCase {
    
    func test_aiff_stereo_8bit_44100() throws {
        let path = try load(resource: "aiff_stereo_8bit_44100", ofType: "aif")
        let sut = makeSUT()
        assert(that: sut, with: path, samplesPerChannel: 352800, bitDepth: 8, sampleRate: 44100, numChannels: 2)
    }
    
    func test_aiff_stereo_8bit_48000() throws {
        let path = try load(resource: "aiff_stereo_8bit_48000", ofType: "aif")
        let sut = makeSUT()
        assert(that: sut, with: path, samplesPerChannel: 384000, bitDepth: 8, sampleRate: 48000, numChannels: 2)
    }
    
    func test_aiff_stereo_16bit_44100() throws {
        let path = try load(resource: "aiff_stereo_16bit_44100", ofType: "aif")
        let sut = makeSUT()
        assert(that: sut, with: path, samplesPerChannel: 352800, bitDepth: 16, sampleRate: 44100, numChannels: 2)
    }
    
    func test_aiff_stereo_16bit_48000() throws {
        let path = try load(resource: "aiff_stereo_16bit_48000", ofType: "aif")
        let sut = makeSUT()
        assert(that: sut, with: path, samplesPerChannel: 384000, bitDepth: 16, sampleRate: 48000, numChannels: 2)
    }
    
    func test_aiff_stereo_24bit_44100() throws {
        let path = try load(resource: "aiff_stereo_24bit_44100", ofType: "aif")
        let sut = makeSUT()
        assert(that: sut, with: path, samplesPerChannel: 352800, bitDepth: 24, sampleRate: 44100, numChannels: 2)
    }
    
    func test_aiff_stereo_24bit_48000() throws {
        let path = try load(resource: "aiff_stereo_24bit_48000", ofType: "aif")
        let sut = makeSUT()
        assert(that: sut, with: path, samplesPerChannel: 384000, bitDepth: 24, sampleRate: 48000, numChannels: 2)
    }
    
    func test_aiff_stereo_32bit_44100() throws {
        let path = try load(resource: "aiff_stereo_32bit_44100", ofType: "aif")
        let sut = makeSUT()
        assert(that: sut, with: path, samplesPerChannel: 384873, bitDepth: 32, sampleRate: 44100, numChannels: 2)
    }
    
    func test_aiff_stereo_32bit_48000() throws {
        let path = try load(resource: "aiff_stereo_32bit_48000", ofType: "aif")
        let sut = makeSUT()
        assert(that: sut, with: path, samplesPerChannel: 418909, bitDepth: 32, sampleRate: 48000, numChannels: 2)
    }
    
    // MARK: - Helper
    
    private func makeSUT(file: StaticString = #filePath, line: UInt = #line) -> AudioWrapper {
        let sut = AudioWrapper()
        trackForMemoryLeaks(sut, file: file, line: line)
        return sut
    }
}
