//
//  XCTestCase+Extensions.swift
//
//
//  Created by Massimiliano Bonafede on 14/05/24.
//

import XCTest
@testable import SwiftWrapper

extension XCTestCase {
    func trackForMemoryLeaks(_ instance: AnyObject, file: StaticString = #filePath, line: UInt = #line) {
        addTeardownBlock { [weak instance] in
            XCTAssertNil(instance, "Instance should have been deallocated. Potential memory leak.", file: file, line: line)
        }
    }
    
    func load(resource: String, ofType: String) throws -> String {
        guard let path = Bundle.module.path(forResource: resource, ofType: ofType) else {
            throw NSError(domain: "Resource \(resource).\(ofType) not found", code: -1)
        }
        return path
    }
    
    func assert(that sut: AudioWrapper, with path: String, samplesPerChannel: Int32, bitDepth: Int32, sampleRate: Int32, numChannels: Int32, file: StaticString = #filePath, line: UInt = #line) {
        XCTAssertTrue(sut.load(path), file: file, line: line)
        XCTAssertEqual(sut.getNumSamplesPerChannel(), samplesPerChannel, file: file, line: line)
        XCTAssertEqual(sut.getBitDepth(), bitDepth, file: file, line: line)
        XCTAssertEqual(sut.getSampleRate(), sampleRate, file: file, line: line)
        XCTAssertEqual(sut.getNumChannels(), numChannels, file: file, line: line)
    }
}
