//
//  WavAudioTests.swift
//  
//
//  Created by Massimiliano Bonafede on 16/05/24.
//

import XCTest
@testable import SwiftWrapper

final class WavAudioTests: XCTestCase {

    func test_wav_8chan_24bit_48000() throws {
        let path = try load(resource: "wav_8chan_24bit_48000", ofType: "wav")
        let sut = makeSUT()
        assert(that: sut, with: path, samplesPerChannel: 191524, bitDepth: 24, sampleRate: 48000, numChannels: 8)
    }
    
    func test_wav_mono_16bit_44100() throws {
        let path = try load(resource: "wav_mono_16bit_44100", ofType: "wav")
        let sut = makeSUT()
        assert(that: sut, with: path, samplesPerChannel: 352800, bitDepth: 16, sampleRate: 44100, numChannels: 1)
    }
    
    func test_wav_mono_16bit_48000() throws {
        let path = try load(resource: "wav_mono_16bit_48000", ofType: "wav")
        let sut = makeSUT()
        assert(that: sut, with: path, samplesPerChannel: 384000, bitDepth: 16, sampleRate: 48000, numChannels: 1)
    }
    
    func test_wav_stereo_8bit_44100() throws {
        let path = try load(resource: "wav_stereo_8bit_44100", ofType: "wav")
        let sut = makeSUT()
        assert(that: sut, with: path, samplesPerChannel: 352800, bitDepth: 8, sampleRate: 44100, numChannels: 2)
    }
    
    func test_wav_stereo_8bit_48000() throws {
        let path = try load(resource: "wav_stereo_8bit_48000", ofType: "wav")
        let sut = makeSUT()
        assert(that: sut, with: path, samplesPerChannel: 384000, bitDepth: 8, sampleRate: 48000, numChannels: 2)
    }
    
    func test_wav_stereo_16bit_44100() throws {
        let path = try load(resource: "wav_stereo_16bit_44100", ofType: "wav")
        let sut = makeSUT()
        assert(that: sut, with: path, samplesPerChannel: 352800, bitDepth: 16, sampleRate: 44100, numChannels: 2)
    }
    
    func test_wav_stereo_16bit_48000() throws {
        let path = try load(resource: "wav_stereo_16bit_48000", ofType: "wav")
        let sut = makeSUT()
        assert(that: sut, with: path, samplesPerChannel: 384000, bitDepth: 16, sampleRate: 48000, numChannels: 2)
    }
    
    func test_wav_stereo_24bit_44100() throws {
        let path = try load(resource: "wav_stereo_24bit_44100", ofType: "wav")
        let sut = makeSUT()
        assert(that: sut, with: path, samplesPerChannel: 352800, bitDepth: 24, sampleRate: 44100, numChannels: 2)
    }
    
    func test_wav_stereo_24bit_48000() throws {
        let path = try load(resource: "wav_stereo_24bit_48000", ofType: "wav")
        let sut = makeSUT()
        assert(that: sut, with: path, samplesPerChannel: 384000, bitDepth: 24, sampleRate: 48000, numChannels: 2)
    }
    
    func test_wav_stereo_32bit_44100() throws {
        let path = try load(resource: "wav_stereo_32bit_44100", ofType: "wav")
        let sut = makeSUT()
        assert(that: sut, with: path, samplesPerChannel: 384873, bitDepth: 32, sampleRate: 44100, numChannels: 2)
    }
    
    func test_wav_stereo_32bit_48000() throws {
        let path = try load(resource: "wav_stereo_32bit_48000", ofType: "wav")
        let sut = makeSUT()
        assert(that: sut, with: path, samplesPerChannel: 418909, bitDepth: 32, sampleRate: 48000, numChannels: 2)
    }
    
    // MARK: - Helper
    
    private func makeSUT(file: StaticString = #filePath, line: UInt = #line) -> AudioWrapper {
        let sut = AudioWrapper()
        trackForMemoryLeaks(sut, file: file, line: line)
        return sut
    }
}
