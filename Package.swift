// swift-tools-version: 5.10
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "AudioWrapper",
    products: [
        .library(
            name: "AudioWrapper",
            targets: ["SwiftWrapper", "ObjCWrapper"])
    ],
    targets: [
        .target(name: "ObjCWrapper",
                path: "Sources/ObjCWrapper/",
                publicHeadersPath: "../ObjCWrapper/",
                cxxSettings: [
                    .headerSearchPath("../CPP/")
                ]),
        .target(
            name: "SwiftWrapper",
            dependencies: ["ObjCWrapper"],
            path: "Sources/SwiftWrapper/"),
        .testTarget(
            name: "AudioWrapperTests",
            dependencies: ["SwiftWrapper"], 
            resources: [.process("Resources")]),
    ], cxxLanguageStandard: .cxx20
)
